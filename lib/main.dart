import 'package:flutter/material.dart';
import 'package:roll_dice/gradient_container.dart';

void main() {
  runApp(
    const MaterialApp(
      home: Scaffold(
        // backgroundColor: const Color.fromARGB(255, 184, 238, 245),
        body: GradientContainer(Colors.deepPurple, Colors.blueAccent),
      ),
    ),
  );
}
